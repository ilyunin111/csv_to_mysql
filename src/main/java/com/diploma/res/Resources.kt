package com.diploma.res

import java.io.File
import java.nio.file.Paths

class Resources(private val dir: File) {

    constructor(path: String) : this(Unit.javaClass.getResource(path).toURI().let { Paths.get(it).toFile() })

    init {
        if (!dir.isDirectory) {
            throw IllegalStateException("$dir is not directory")
        }
    }

    fun all(): List<File> = dir.listFiles()?.asList() ?: emptyList()

}