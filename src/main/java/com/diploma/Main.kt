package com.diploma

import com.diploma.csv.Csv
import com.diploma.res.Resources
import com.diploma.search.SearchEntryFactory
import com.diploma.search.SearchEntryRepositoryFactory

fun main() {
    val factory = SearchEntryFactory()
    val repository = SearchEntryRepositoryFactory().create()
    val csvs = Resources("/csv").all()
    csvs
        .map(::Csv)
        .forEach { csv ->
            csv.iterator().asSequence()
                .map(factory::create)
                .toList()
                .apply(repository::addAll)
        }
}
