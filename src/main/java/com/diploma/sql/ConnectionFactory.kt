package com.diploma.sql

import java.sql.Connection
import java.sql.DriverManager

class ConnectionFactory {

    fun create(): Connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD)

    companion object {
        private const val USER_NAME = "root"
        private const val PASSWORD = "root"
        private const val URL = "jdbc:mysql://localhost:3306/diploma"

        init {
            Class.forName("com.mysql.cj.jdbc.Driver")
        }
    }

}