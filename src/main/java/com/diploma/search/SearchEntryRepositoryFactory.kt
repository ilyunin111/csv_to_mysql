package com.diploma.search

import com.diploma.sql.ConnectionFactory

class SearchEntryRepositoryFactory {

    fun create(): SearchEntryRepository = SearchEntryJdbcRepository(ConnectionFactory())

}