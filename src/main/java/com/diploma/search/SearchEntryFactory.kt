package com.diploma.search

import com.diploma.csv.CsvRow

class SearchEntryFactory {

    fun create(row: CsvRow) = SearchEntry(
        row[1].toInt(),
        row[2],
        row[3],
        row[4],
        row[5],
        row[7].toLong()
    )

}