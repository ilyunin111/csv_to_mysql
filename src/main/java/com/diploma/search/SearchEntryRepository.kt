package com.diploma.search

interface SearchEntryRepository {

    fun addAll(entries: List<SearchEntry>)

}