package com.diploma.search

data class SearchEntry(
    val rank: Int,
    val title: String,
    val snippet: String,
    val displayLink: String,
    val link: String,
    val totalResults: Long
)