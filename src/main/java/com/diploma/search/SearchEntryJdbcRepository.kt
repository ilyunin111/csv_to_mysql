package com.diploma.search

import com.diploma.sql.ConnectionFactory
import java.sql.Connection

class SearchEntryJdbcRepository(private val connectionFactory: ConnectionFactory) : SearchEntryRepository {

    override fun addAll(entries: List<SearchEntry>) {
        connectionFactory.create().use { connection ->
            entries.forEach { entry -> writeEntry(entry, connection) }
        }
    }

    private fun writeEntry(entry: SearchEntry, connection: Connection) {
        connection.prepareStatement(INSERT_STATEMENT).use {
            it.apply {
                setInt(1, entry.rank)
                setString(2, entry.title)
                setString(3, entry.snippet)
                setString(4, entry.displayLink)
                setString(5, entry.link)
                setLong(6, entry.totalResults)
                execute()
            }
        }
    }

    companion object {
        private const val INSERT_STATEMENT =
            "INSERT " +
                    "INTO `search`(`rank`, `title`, `snippet`, `display_link`, `link`, `total_results`) " +
                    "VALUES(?, ?, ?, ?, ?, ?)"
    }
}