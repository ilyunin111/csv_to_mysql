package com.diploma.csv

import com.opencsv.CSVReader
import java.io.File

class Csv(private val file: File) : Iterable<CsvRow> {

    override fun iterator(): Iterator<CsvRow> =
        CsvIterator(CSVReader(file.bufferedReader()).iterator())

    private class CsvIterator(private val fileIterator: Iterator<Array<String>>) : Iterator<CsvRow> {

        init {
            fileIterator.next()
        }

        override fun hasNext() = fileIterator.hasNext()

        override fun next() = CsvRow(fileIterator.next())

    }

}