package com.diploma.csv

class CsvRow(private val row: Array<String>) {
    operator fun get(index: Int) = row[index]
}